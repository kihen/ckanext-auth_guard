<img src="https://www.dtu.dk/-/media/DTUdk/Logo/logo-web-small" align="left" width="73px" height="90px" />

# ckanext-auth_guard

This extensions allows you to disable anonymous access to your CKAN site and API.


## Requirements

Tested on CKAN 2.9.


## Installation

To install ckanext-auth_guard:

1. Activate your CKAN virtual environment, for example:

    ```
    . /usr/lib/ckan/default/bin/activate
    ```

2. Clone the source and install it on the virtualenv

    ```
    git clone https://gitlab.gbar.dtu.dk/kihen/ckanext-auth_guard
    cd ckanext-auth_guard
    pip install -e .
	pip install -r requirements.txt
    ```

3. Add `auth_guard` to the `ckan.plugins` setting in your CKAN
   config file (by default the config file is located at
   `/etc/ckan/default/ckan.ini`).

4. Restart CKAN. For example if you've deployed CKAN with Apache on Ubuntu:

    ```
    sudo service apache2 reload
    ```


## Config settings

Name|Description|Options|Default
--|--|--|--
`ckanext.auth_guard.unguarded_paths`|A space seperated list of unguarded paths.||/favicon.ico /error/document /user/login /ldap_login_handler /user/logged_in /user/logged_out /user/logged_out_redirect /util/redirect


## Developer installation

To install ckanext-auth_guard for development, activate your CKAN virtualenv and
do:

``` 
git clone https://gitlab.gbar.dtu.dk/ckan/ckanext-auth_guard.git
cd ckanext-auth_guard
python setup.py develop
pip install -r dev-requirements.txt
```

## Tests

To run the tests, do:

```
pytest --ckan-ini=test.ini
```

## Releasing a new version of ckanext-auth_guard

If ckanext-auth_guard should be available on PyPI you can follow these steps to publish a new version:

1. Update the version number in the `setup.py` file. See [PEP 440](http://legacy.python.org/dev/peps/pep-0440/#public-version-identifiers) for how to choose version numbers.

2. Make sure you have the latest version of necessary packages:

    ```
    pip install --upgrade setuptools wheel twine
    ```

3. Create a source and binary distributions of the new version:

    ```
    python setup.py sdist bdist_wheel && twine check dist/*
    ```

    Fix any errors you get.

4. Upload the source distribution to PyPI:

    ```
    twine upload dist/*
    ```

5. Commit any outstanding changes:

    ```
    git commit -a
    git push
    ```

6. Tag the new release of the project on GitHub with the version number from
   the `setup.py` file. For example if the version number in `setup.py` is
   0.0.1 then do:

    ```
    git tag 0.0.1
    git push --tags
    ```

## License

[AGPL](https://www.gnu.org/licenses/agpl-3.0.en.html)

