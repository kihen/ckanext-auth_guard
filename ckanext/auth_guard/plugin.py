from ckan.common import config, request, session
from flask import redirect, Response
import ckan.plugins as plugins
import ckan.model as model
import ckan.lib.base as base
import ckan.lib.api_token as api_token
from ckan.lib.helpers import url_for
import logging

logger = logging.getLogger(__name__)
UNGUARDED_PATHS =  ['/favicon.ico', '/error/document', '/user/login', 
                    '/ldap_login_handler', '/user/logged_in', '/user/logged_out', 
                    '/user/logged_out_redirect', '/util/redirect']

class AuthGuardPlugin(plugins.SingletonPlugin):
    
    plugins.implements(plugins.IMiddleware, inherit=True)

    def make_middleware(self, app, config):

        @app.before_request
        def before_request_callback():
            # We allow access if the client is requesting a path that is not
            # guarded, otherwise the client has be authorized but otherwise,
            # the client has be authorized.
            if self.is_guarded_path() and not self.is_authorized():
                if self.is_api_request():
                    return self.unauthorized_response()
                else:
                    return self.redirect_response()

        return app


    def is_guarded_path(self):
        # We check if the client is requesting a non-guarded
        if (request.environ['PATH_INFO'].startswith('/webassets/')
            or request.environ['PATH_INFO'].startswith('/base/')
            or request.environ['PATH_INFO'].startswith('/api/i18n/')
            or request.environ['PATH_INFO'] in self.get_unguarded_paths()):
            return False

        return True

    def redirect_response(self):
        # Resolve the login page url
        url = url_for('user.login', came_from=request.environ['PATH_INFO'])

        if url.startswith('/'):
            url = str(config['ckan.site_url'].rstrip('/') + url)

        logger.info('Redirect client to: ' + url)

        # Redirect the client to the login page
        return redirect(
            url,
            code=307
        )

    def unauthorized_response(self):
        # Return 400 Bad Request response (with additional JSON payload)
        return Response(
            '{"success": false, "error": "Invalid or missing authorization token"}',
            status=400,
            mimetype='application/json'
        )

    def get_unguarded_paths(self):
        # Get the value of the ckan.auth_guard.unguarded_paths setting from the
        # CKAN config file or the default list defined in the UNGUARDED_PATHS
        # variable, if the setting isn't in the config file.
        return plugins.toolkit.aslist(config.get('ckanext.auth_guard.unguarded_paths', UNGUARDED_PATHS))

    def is_authorized(self):
        # Make sure the client is either logged in or provided a valid API key
        if (self.is_logged_in()
            or (self.is_api_request() and self.check_api_key())):
            return True

        return False

    def is_api_request(self):
        # If an API key is available, we assume it's an API request
        return self.get_client_api_key() is not None

    def is_logged_in(self):
        # Check if the client is authenticated
        if ('repoze.who.identity' in request.environ
            or 'ckanext-ldap-user' in session):
            return True

        return False

    def check_api_key(self):
        # Get the client API key
        api_key = self.get_client_api_key()

        # Validate the API key
        if api_key is not None:
            if api_token.get_user_from_token(api_key):
                return True

            query = model.Session.query(model.User)

            if query.filter_by(apikey=api_key).count() > 0:
                return True

        return False

    def get_client_api_key(self):
        # Get the user defined API key header name
        header = config.get(
            base.APIKEY_HEADER_NAME_KEY,
            base.APIKEY_HEADER_NAME_DEFAULT
        )

        # Get the API key
        api_key = request.environ.get('HTTP_' + header.replace('-', '_').upper(), '')

        # If we didn't get an API key, we look in the Authorization header
        if not api_key:
            api_key = request.environ.get('HTTP_AUTHORIZATION', '')
            # We ignore the Authorization header if it contains whitespaces
            if ' ' in api_key:
                api_key = ''

        if not api_key:
            return None

        return str(api_key)
